.. scoringduck documentation master file, created by
   sphinx-quickstart on Thu Dec 17 15:23:52 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to scoringduck's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/introduction
   usage/functionalities
   usage/tutorial


   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


